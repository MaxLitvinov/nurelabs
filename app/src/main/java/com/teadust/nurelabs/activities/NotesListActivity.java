package com.teadust.nurelabs.activities;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.teadust.nurelabs.R;
import com.teadust.nurelabs.adapters.NoteAdapter;
import com.teadust.nurelabs.classes.Note;

import java.util.ArrayList;
import java.util.List;

public class NotesListActivity extends AppCompatActivity {

    private static final int REQUESTED_CODE_ADD_NOTE = 1;
    private static final int REQUESTED_CODE_EDIT_NOTE = 2;

    public static class NoteValue {
        public static final String IMAGE = "image";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String IMPORTANCE = "importance";
        public static final String CREATED_TIME = "createdTime";
        public static final String APPOINTED_DATE = "appointedDate";
        public static final String APPOINTED_TIME = "appointedTime";
    }

    private List<Note> notesList;

    private NoteAdapter adapter;

    private ListView lvNotesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        notesList = new ArrayList<Note>();

        adapter = new NoteAdapter(this, notesList);

        lvNotesList = (ListView) findViewById(R.id.lv_notes_list);
        adapter.notifyDataSetChanged();
        lvNotesList.setAdapter(adapter);

        registerForContextMenu(lvNotesList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_list_main_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(NotesListActivity.this, "onQueryTextSubmit", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Toast.makeText(NotesListActivity.this, "onQueryTextChange", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent addNoteIntent = new Intent(NotesListActivity.this, AddNoteActivity.class);
                startActivityForResult(addNoteIntent, REQUESTED_CODE_ADD_NOTE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUESTED_CODE_EDIT_NOTE) {
            if (resultCode == RESULT_OK) {
                long id = data.getLongExtra("id", -1);
                byte[] image = data.getByteArrayExtra(NoteValue.IMAGE);
                String title = data.getStringExtra(NoteValue.TITLE);
                String description = data.getStringExtra(NoteValue.DESCRIPTION);
                String importance = data.getStringExtra(NoteValue.IMPORTANCE);
                String createdTime = data.getStringExtra(NoteValue.CREATED_TIME);
                String appointedTime = data.getStringExtra(NoteValue.APPOINTED_DATE);
                String appointedDate = data.getStringExtra(NoteValue.APPOINTED_TIME);

                Note note = new Note();
                note.setImage(image);
                note.setTitle(title);
                note.setDescription(description);
                note.setImportance(getImportanceFromString(importance));
                note.setCreatedTime(createdTime);
                note.setAppointedDate(appointedDate);
                note.setAppointedTime(appointedTime);

                notesList.remove((int) id);
                notesList.add((int) id, note);
                adapter = new NoteAdapter(this, notesList);
                lvNotesList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == REQUESTED_CODE_ADD_NOTE) {
            if (resultCode == RESULT_OK) {
                byte[] image = data.getByteArrayExtra(NoteValue.IMAGE);
                String title = data.getStringExtra(NoteValue.TITLE);
                String description = data.getStringExtra(NoteValue.DESCRIPTION);
                String importance = data.getStringExtra(NoteValue.IMPORTANCE);
                String createdTime = data.getStringExtra(NoteValue.CREATED_TIME);
                String appointedTime = data.getStringExtra(NoteValue.APPOINTED_DATE);
                String appointedDate = data.getStringExtra(NoteValue.APPOINTED_TIME);

                Note note = new Note();
                note.setImage(image);
                note.setTitle(title);
                note.setDescription(description);
                note.setImportance(getImportanceFromString(importance));
                note.setCreatedTime(createdTime);
                note.setAppointedDate(appointedDate);
                note.setAppointedTime(appointedTime);

                notesList.add(note);
                adapter = new NoteAdapter(this, notesList);
                lvNotesList.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(android.R.drawable.arrow_down_float);
        menu.setHeaderTitle("Choose action");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_list_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit:
                long id = info.id;
                byte[] image = notesList.get((int) info.id).getImage();
                String title = notesList.get((int) info.id).getTitle();
                String description = notesList.get((int) info.id).getDescription();
                int importance = notesList.get((int) info.id).getImportance();
                String createdTime = notesList.get((int) info.id).getCreatedTime();
                String appointedDate = notesList.get((int) info.id).getAppointedDate();
                String appointedTime = notesList.get((int) info.id).getAppointedTime();

                Intent editIntent = new Intent(NotesListActivity.this, EditNoteActivity.class);
                editIntent.putExtra("id", id);
                editIntent.putExtra(NoteValue.IMAGE, image);
                editIntent.putExtra(NoteValue.TITLE, title);
                editIntent.putExtra(NoteValue.DESCRIPTION, description);
                editIntent.putExtra(NoteValue.IMPORTANCE, importance);
                editIntent.putExtra(NoteValue.CREATED_TIME, createdTime);
                editIntent.putExtra(NoteValue.APPOINTED_DATE, appointedDate);
                editIntent.putExtra(NoteValue.APPOINTED_TIME, appointedTime);
                startActivityForResult(editIntent, REQUESTED_CODE_EDIT_NOTE);
                return true;
            case R.id.delete:
                showDeleteDialog(info.id);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showDeleteDialog(final long id) {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle("Deleting");
        deleteDialog.setMessage("Are you sure want to delete this note?");
        deleteDialog.setIcon(android.R.drawable.ic_menu_delete);

        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notesList.remove((int) id);
                adapter.notifyDataSetChanged();
            }
        });

        deleteDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        deleteDialog.show();
    }

    private Note.Importance getImportanceFromString(String importance) {
        switch (importance) {
            case "Low":
                return Note.Importance.LOW;
            case "Middle":
                return Note.Importance.MIDDLE;
            case "High":
                return Note.Importance.HIGH;
            default:
                return Note.Importance.LOW;
        }
    }
}
