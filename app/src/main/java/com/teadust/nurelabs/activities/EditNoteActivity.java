package com.teadust.nurelabs.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.teadust.nurelabs.R;
import com.teadust.nurelabs.adapters.ImportanceSpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditNoteActivity extends AppCompatActivity {

    private final int PICK_IMAGE_CODE = 1;

    private ImageView noteImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        Intent noteInfoIntent = getIntent();

        final long id = noteInfoIntent.getLongExtra("id", -1);
        Log.d("---", String.valueOf(id));
        byte[] image = noteInfoIntent.getByteArrayExtra(NotesListActivity.NoteValue.IMAGE);
        String title = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.TITLE);
        String description = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.DESCRIPTION);
        String importance = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.IMPORTANCE);
        String createdTime = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.CREATED_TIME);

        String appointedDate = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.APPOINTED_DATE);
        int year = 0;
        int month = 0;
        int day = 0;
        try {
            year = getYear(appointedDate);
            month = getMonth(appointedDate);
            day = getDay(appointedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String appointedTime = noteInfoIntent.getStringExtra(NotesListActivity.NoteValue.APPOINTED_TIME);
        int hour = 0;
        int minute = 0;
        try {
            hour = getHour(appointedTime);
            minute = getMinute(appointedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        noteImage = (ImageView) findViewById(R.id.iv_note_image);
        final EditText noteTitle = (EditText) findViewById(R.id.et_note_title);
        final EditText noteDescription = (EditText) findViewById(R.id.et_note_description);
        final Spinner noteImportance = (Spinner) findViewById(R.id.spn_importance);
        final DatePicker noteAppointedDate = (DatePicker) findViewById(R.id.dp_appointed_date);
        final TimePicker noteAppointedTime = (TimePicker) findViewById(R.id.tp_appointed_time);
        noteAppointedTime.setIs24HourView(true);

        noteImage.setImageBitmap(convertByteToBitmap(image));
        noteTitle.setText(title);
        noteDescription.setText(description);
        noteImportance.setPrompt(importance);
        noteAppointedDate.updateDate(year, month, day);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            noteAppointedTime.setHour(hour);
            noteAppointedTime.setMinute(minute);
        } else {
            noteAppointedTime.setCurrentHour(hour);
            noteAppointedTime.setCurrentMinute(minute);
        }

        noteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageIntent = new Intent(Intent.ACTION_PICK);
                getImageIntent.setType("image/*");
                startActivityForResult(getImageIntent, PICK_IMAGE_CODE);
            }
        });

        noteImportance.setAdapter(new ImportanceSpinnerAdapter(this));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        final String currentDateAndTime = simpleDateFormat.format(new Date());

        Button btnSaveChanges = (Button) findViewById(R.id.btn_save_changes);
        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] image = convertImageToByte(noteImage);
                String title = noteTitle.getText().toString();
                String description = noteDescription.getText().toString();
                String importance = noteImportance.getSelectedItem().toString();
                String appointedDate = getDate(noteAppointedDate);
                String appointedTime = getTime(noteAppointedTime);

                Intent backData = new Intent();
                backData.putExtra("id", id);
                backData.putExtra(NotesListActivity.NoteValue.IMAGE, image);
                backData.putExtra(NotesListActivity.NoteValue.TITLE, title);
                backData.putExtra(NotesListActivity.NoteValue.DESCRIPTION, description);
                backData.putExtra(NotesListActivity.NoteValue.IMPORTANCE, importance);
                backData.putExtra(NotesListActivity.NoteValue.CREATED_TIME, currentDateAndTime);
                backData.putExtra(NotesListActivity.NoteValue.APPOINTED_DATE, appointedDate);
                backData.putExtra(NotesListActivity.NoteValue.APPOINTED_TIME, appointedTime);

                setResult(RESULT_OK, backData);

                finish();
            }
        });
    }

    private byte[] convertImageToByte(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bmp = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private Bitmap convertByteToBitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    private String getDate(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        return day + "." + month + "." + year;
    }

    private String getTime(TimePicker timePicker) {
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();

        return hour + ":" + minute;
    }

    private Calendar getFullDate(String initialFullDate) throws ParseException {
        SimpleDateFormat pattern = new SimpleDateFormat("dd.MM.yyyy");
        Date date = pattern.parse(initialFullDate);
        return Calendar.getInstance();
    }

    private int getYear(String initialDate) throws ParseException {
        return getFullDate(initialDate).get(Calendar.YEAR);
    }

    private int getMonth(String initialDate) throws ParseException {
        return getFullDate(initialDate).get(Calendar.MONTH);
    }

    private int getDay(String initialDate) throws ParseException {
        return getFullDate(initialDate).get(Calendar.DAY_OF_MONTH);
    }

    private Calendar getFullTime(String initialFullTime) throws ParseException {
        SimpleDateFormat pattern = new SimpleDateFormat("HH:mm");
        Date date = pattern.parse(initialFullTime);
        return Calendar.getInstance();
    }

    private int getHour(String initialTime) throws ParseException {
        return getFullTime(initialTime).get(Calendar.HOUR_OF_DAY);
    }

    private int getMinute(String initialTime) throws ParseException {
        return getFullTime(initialTime).get(Calendar.MINUTE);
    }

    private String getImportance(int importance) {
        switch (importance) {
            case 1:
                return "Low";
            case 2:
                return "Middle";
            case 3:
                return "High";
            default:
                return "Low";
        }
    }
}
