package com.teadust.nurelabs.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.teadust.nurelabs.R;

public class ColorDisplayActivity extends AppCompatActivity {

    private LinearLayout colorPanel;

    private SeekBar sbRed;
    private SeekBar sbGreen;
    private SeekBar sbBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_display);

        colorPanel = (LinearLayout) findViewById(R.id.color_panel);

        sbRed = (SeekBar) findViewById(R.id.sb_red);
        sbGreen = (SeekBar) findViewById(R.id.sb_green);
        sbBlue = (SeekBar) findViewById(R.id.sb_blue);

        sbRed.setProgress(231);
        sbGreen.setProgress(151);
        sbBlue.setProgress(51);

        sbRed.setOnSeekBarChangeListener(onSeekBarChangeListener);
        sbGreen.setOnSeekBarChangeListener(onSeekBarChangeListener);
        sbBlue.setOnSeekBarChangeListener(onSeekBarChangeListener);

        updateBackground();
    }

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            updateBackground();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private void updateBackground() {
        int red = sbRed.getProgress();
        int green = sbGreen.getProgress();
        int blue = sbBlue.getProgress();
        colorPanel.setBackgroundColor(0xff000000 + red * 0x10000 + green * 0x100 + blue);
    }
}
