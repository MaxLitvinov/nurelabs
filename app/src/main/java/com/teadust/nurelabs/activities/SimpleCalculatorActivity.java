package com.teadust.nurelabs.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teadust.nurelabs.R;

public class SimpleCalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView digitalPanel;

    private int result = 0;
    private String initialString = "0";
    private char lastOperator = ' ';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_calculator);

        digitalPanel = (TextView) findViewById(R.id.digital_panel);

        Button btn0 = (Button) findViewById(R.id.btn0);
        Button btn1 = (Button) findViewById(R.id.btn1);
        Button btn2 = (Button) findViewById(R.id.btn2);
        Button btn3 = (Button) findViewById(R.id.btn3);
        Button btn4 = (Button) findViewById(R.id.btn4);
        Button btn5 = (Button) findViewById(R.id.btn5);
        Button btn6 = (Button) findViewById(R.id.btn6);
        Button btn7 = (Button) findViewById(R.id.btn7);
        Button btn8 = (Button) findViewById(R.id.btn8);
        Button btn9 = (Button) findViewById(R.id.btn9);
        Button btnDivide = (Button) findViewById(R.id.btn_divide);
        Button btnMultiple = (Button) findViewById(R.id.btn_multiple);
        Button btnMinus = (Button) findViewById(R.id.btn_minus);
        Button btnPlus = (Button) findViewById(R.id.btn_plus);
        Button btnClear = (Button) findViewById(R.id.btn_clear);
        Button btnEquals = (Button) findViewById(R.id.btn_equals);

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnDivide.setOnClickListener(this);
        btnMultiple.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnEquals.setOnClickListener(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        digitalPanel.setText(savedInstanceState.getString("digitalPanel"));
        result = savedInstanceState.getInt("result");
        initialString = savedInstanceState.getString("initialString");
        lastOperator = savedInstanceState.getChar("lastOperator");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("digitalPanel", digitalPanel.getText().toString());
        outState.putInt("result", result);
        outState.putString("initialString", initialString);
        outState.putChar("lastOperator", lastOperator);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn0:
            case R.id.btn1:
            case R.id.btn2:
            case R.id.btn3:
            case R.id.btn4:
            case R.id.btn5:
            case R.id.btn6:
            case R.id.btn7:
            case R.id.btn8:
            case R.id.btn9:
                String initialDigit = ((Button) v).getText().toString();
                if (initialString.equals("0")) {
                    initialString = initialDigit;
                } else {
                    initialString += initialDigit;
                }
                digitalPanel.setText(initialString);
                if (lastOperator == '=') {
                    result = 0;
                    lastOperator = ' ';
                }
                break;
            case R.id.btn_divide:
                compute();
                lastOperator = '/';
                break;
            case R.id.btn_multiple:
                compute();
                lastOperator = '*';
                break;
            case R.id.btn_minus:
                compute();
                lastOperator = '-';
                break;
            case R.id.btn_plus:
                compute();
                lastOperator = '+';
                break;
            case R.id.btn_clear:
                result = 0;
                initialString = "0";
                lastOperator = ' ';
                digitalPanel.setText("0");
                break;
            case R.id.btn_equals:
                compute();
                lastOperator = '=';
                break;
        }
    }

    private void compute() {
        int initialNumber = Integer.parseInt(initialString);
        initialString = "0";
        if (lastOperator == ' ') {
            result = initialNumber;
        } else if (lastOperator == '+') {
            result += initialNumber;
        } else if (lastOperator == '-') {
            result -= initialNumber;
        } else if (lastOperator == '*') {
            result *= initialNumber;
        } else if (lastOperator == '/') {
            result /= initialNumber;
        } else if (lastOperator == '=') {

        }
        digitalPanel.setText(String.valueOf(result));
    }
}
