package com.teadust.nurelabs.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.teadust.nurelabs.R;
import com.teadust.nurelabs.adapters.ImportanceSpinnerAdapter;
import com.teadust.nurelabs.classes.DateTimePicker;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddNoteActivity extends AppCompatActivity implements DateTimePicker.OnDateTimeSetListener {

    private final int PICK_IMAGE_CODE = 1;

    private ImageView noteImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        noteImage = (ImageView) findViewById(R.id.iv_note_image);
        final EditText noteTitle = (EditText) findViewById(R.id.et_note_title);
        final EditText noteDescription = (EditText) findViewById(R.id.et_note_description);
        final Spinner noteImportance = (Spinner) findViewById(R.id.spn_importance);
        final DatePicker noteAppointedDate = (DatePicker) findViewById(R.id.dp_appointed_date);
        final TimePicker noteAppointedTime = (TimePicker) findViewById(R.id.tp_appointed_time);
        noteAppointedTime.setIs24HourView(true);

        noteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageIntent = new Intent(Intent.ACTION_PICK);
                getImageIntent.setType("image/*");
                startActivityForResult(getImageIntent, PICK_IMAGE_CODE);
            }
        });

        noteImportance.setAdapter(new ImportanceSpinnerAdapter(this));

        SimpleDateFormat pattern = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        final String currentDateAndTimeFormatter = pattern.format(new Date());

        Button btnAddNote = (Button) findViewById(R.id.btn_save_changes);
        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SimpleDateTimePicker simpleDateTimePicker = SimpleDateTimePicker.make(
//                        "Choose Date and Time",
//                        new Date(),
//                        AddNoteActivity.this,
//                        getSupportFragmentManager());
//
//                simpleDateTimePicker.show();

                byte[] image = convertImageToByte(noteImage);
                String title = noteTitle.getText().toString();
                String description = noteDescription.getText().toString();
                String importance = noteImportance.getSelectedItem().toString();
                String appointedDate = getDate(noteAppointedDate);
                String appointedTime = getTime(noteAppointedTime);

                Intent backData = new Intent();
                backData.putExtra(NotesListActivity.NoteValue.IMAGE, image);
                backData.putExtra(NotesListActivity.NoteValue.TITLE, title);
                backData.putExtra(NotesListActivity.NoteValue.DESCRIPTION, description);
                backData.putExtra(NotesListActivity.NoteValue.IMPORTANCE, importance);
                backData.putExtra(NotesListActivity.NoteValue.CREATED_TIME, currentDateAndTimeFormatter);
                backData.putExtra(NotesListActivity.NoteValue.APPOINTED_DATE, appointedDate);
                backData.putExtra(NotesListActivity.NoteValue.APPOINTED_TIME, appointedTime);

                setResult(RESULT_OK, backData);

                finish();
            }
        });
    }

    @Override
    public void DateTimeSet(Date date) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri imageUri =data.getData();
                        InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        noteImage.setImageBitmap(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    private byte[] convertImageToByte(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bmp = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private String getDate(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        return day + "." + month + "." + year;
    }

    private String getTime(TimePicker timePicker) {
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();

        return hour + ":" + minute;
    }
}
