package com.teadust.nurelabs.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.teadust.nurelabs.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton colorDisplayActivity = (ImageButton) findViewById(R.id.ib_color_display);
        colorDisplayActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent colorDisplayIntent = new Intent(MainActivity.this, ColorDisplayActivity.class);
                startActivity(colorDisplayIntent);
            }
        });

        ImageButton simpleCalculatorActivity = (ImageButton) findViewById(R.id.ib_simple_calculator);
        simpleCalculatorActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent simpleCalculatorIntent = new Intent(MainActivity.this, SimpleCalculatorActivity.class);
                startActivity(simpleCalculatorIntent);
            }
        });

        ImageButton notesListActivity = (ImageButton) findViewById(R.id.ib_notes_list);
        notesListActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notesListIntent = new Intent(MainActivity.this, NotesListActivity.class);
                startActivity(notesListIntent);
            }
        });
    }
}
