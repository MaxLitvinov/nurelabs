package com.teadust.nurelabs.classes;

import com.teadust.nurelabs.R;

public class Note {

    private String title;
    private String description;
    private Importance importance;
    private String createdTime;
    private String appointedTime;
    private String appointedDate;
    private byte[] image;

    public Note() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImportance() {
        return importance.getImage();
    }

    public void setImportance(Importance importance) {
        this.importance = importance;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getAppointedTime() {
        return appointedTime;
    }

    public void setAppointedTime(String appointedTime) {
        this.appointedTime = appointedTime;
    }

    public String getAppointedDate() {
        return appointedDate;
    }

    public void setAppointedDate(String appointedDate) {
        this.appointedDate = appointedDate;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public enum Importance {
        LOW (1, R.drawable.notes_list_importance_low),
        MIDDLE (2, R.drawable.notes_list_importance_middle),
        HIGH (3, R.drawable.notes_list_importance_high);

        private int value;
        private int image;

        Importance(int value, int image) {
            this.value = value;
            this.image = image;
        }

        int getImage() {
            return this.image;
        }
    }
}
