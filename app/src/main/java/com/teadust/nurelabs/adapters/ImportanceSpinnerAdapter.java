package com.teadust.nurelabs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teadust.nurelabs.R;

public class ImportanceSpinnerAdapter extends ArrayAdapter<String> {

    private Context context;

    private static String[] importanceTitles = { "Low", "Middle", "High" };

    private int[] imageArray = {
            R.drawable.notes_list_importance_low,
            R.drawable.notes_list_importance_middle,
            R.drawable.notes_list_importance_high
    };

    public ImportanceSpinnerAdapter(Context context) {
        super(context, R.layout.custom_spinner, R.id.tv_spinner_title, importanceTitles);
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImportanceHolder importanceHolder;
        if (convertView == null) {
            importanceHolder = new ImportanceHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.custom_spinner, parent, false);
            importanceHolder.subSpinner = (TextView) convertView.findViewById(R.id.tv_spinner_title);
            importanceHolder.importanceTitle = (TextView) convertView.findViewById(R.id.tv_importance_title);
            importanceHolder.importanceImage = (ImageView) convertView.findViewById(R.id.iv_spinner_image);
            convertView.setTag(importanceHolder);
        } else {
            importanceHolder = (ImportanceHolder) convertView.getTag();
        }
        importanceHolder.subSpinner.setText(importanceTitles[position]);
        importanceHolder.importanceImage.setImageResource(imageArray[position]);
        return convertView;
    }

    private class ImportanceHolder {

        public TextView subSpinner;
        public TextView importanceTitle;
        public ImageView importanceImage;
    }
}