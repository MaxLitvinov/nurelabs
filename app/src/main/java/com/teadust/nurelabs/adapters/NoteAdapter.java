package com.teadust.nurelabs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teadust.nurelabs.R;
import com.teadust.nurelabs.classes.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    public NoteAdapter(Context context, List<Note> notesList) {
        super(context, R.layout.notes_list_note_short_view, notesList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);
        NoteHolder noteHolder;
        if (convertView == null) {
            noteHolder = new NoteHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.notes_list_note_short_view, parent, false);
            noteHolder.noteImage = (ImageView) convertView.findViewById(R.id.iv_note_image);
            noteHolder.noteTitle = (TextView) convertView.findViewById(R.id.tv_note_title);
            noteHolder.noteCreationTime = (TextView) convertView.findViewById(R.id.tv_note_creation_time);
            noteHolder.noteImportance = (ImageView) convertView.findViewById(R.id.iv_note_importance);
            convertView.setTag(noteHolder);
        } else {
            noteHolder = (NoteHolder) convertView.getTag();
        }
        noteHolder.noteImage.setImageResource(R.drawable.notes_list_note_image);
        noteHolder.noteTitle.setText(note.getTitle());
        noteHolder.noteCreationTime.setText(note.getCreatedTime());
        noteHolder.noteImportance.setImageResource(note.getImportance());
        return convertView;
    }

    private class NoteHolder {

        public ImageView noteImage;
        public TextView noteTitle;
        public TextView noteCreationTime;
        public ImageView noteImportance;
    }
}
